// Tutorial: https://dev.to/christianandrei/5-steps-to-change-antd-theme-on-runtime-using-create-react-app-p2k
module.exports = (config, ...rest) => {
  return { ...config, resolve: { ...config.resolve, symlinks: false } };
};
const path = require("path");
const {
  override,
  fixBabelImports,
  addLessLoader,
  addWebpackPlugin,
  addWebpackResolve,
} = require("customize-cra");
const AntDesignThemePlugin = require("antd-theme-webpack-plugin");

const options = {
  stylesDir: path.join(__dirname, "./src/styles"),
  antDir: path.join(__dirname, "./node_modules/antd"),
  varFile: path.join(__dirname, "./src/styles/vars.less"),
  themeVariables: ["@primary-color"],
  indexFileName: "index.html",
};

module.exports = override(
  addWebpackResolve({ symlinks: false }),
  fixBabelImports("antd", {
    libraryName: "antd",
    libraryDirectory: "es",
    style: true,
  }),
  addWebpackPlugin(new AntDesignThemePlugin(options)),
  addLessLoader({
    lessOptions: {
      javascriptEnabled: true,
      modifyVars: {
        //"@primary-color": "#306317", //"#027770", // primary color for all components CUSTOM
        "@link-color": "#1890ff", // link color
        "@success-color": "#52c41a", // success state color
        "@warning-color": "#faad14", // warning state color
        //"@error-color": "#d84a1b", // error state color CUSTOM
        "@border-radius-base": "7px", // major border radius CUSTOM
        "@font-size-base": "16px", // major text font size
        "@heading-color": "rgba(0, 0, 0, 0.85)", // heading text color
        "@text-color": "rgba(0, 0, 0, 0.65)", // major text color
        "@text-color-secondary": "rgba(0, 0, 0, 0.45)", // secondary text color
        "@disabled-color": "rgba(0, 0, 0, 0.25)", // disable state color
        "@border-color-base": "#d9d9d9", // major border color
        "@box-shadow-base":
          "0 3px 6px -4px rgba(0, 0, 0, 0.12), 0 6px 16px 0 rgba(0, 0, 0, 0.08), 0 9px 28px 8px rgba(0, 0, 0, 0.05)", // major shadow for layers
      },
    },
  })
);

import { pbkdf2Sync } from "crypto";

export const baseUrl = "http://localhost:3000";

export const usersUrl = "/users";
export const authUrl = "/auth";
export const deliveriesUrl = "/deliveries";

export const salt = "pE8SNHso^EB9#3H6d8YuH2o4DSEQAj";
export const iterations = 1000;
export const keylen = 16;
export const digest = "sha512";
export const hashPw = (pw: string) =>
  btoa(pbkdf2Sync(pw, salt, iterations, keylen, digest).toString("hex"));

import React from "react";
import { Form, Input, Button, Card, Row, Col } from "antd";
import { Layout } from "antd";
import { Link } from "react-router-dom";
import logo from "../assets/logo.png";
import axios from "axios";
import { CreateUserDTO, UserType, UserRole } from "../shared/types/users.types";
import { baseUrl, hashPw, usersUrl } from "../constants";

const createUser = (user: CreateUserDTO) => {
  axios
    .post(baseUrl + usersUrl, user)
    .then((res) => console.log("res", res))
    .catch((err) => console.log("err", err));
};

export default function Register() {
  const onFinish = (values: any) => {
    createUser({
      firstName: values.firstName,
      lastName: values.lastName,
      email: values.email,
      pwHash: hashPw(values.pw),
      role: UserRole.USER,
      type: UserType.STUDENT,
    });
  };

  const { Content } = Layout;

  const onFinishFailed = (errorInfo: any) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <>
      <Content style={{ minHeight: "100vh" }}>
        <Row
          style={{
            height: "calc(5em + 20vh)",
            alignItems: "flex-end",
            justifyContent: "center",
          }}
        >
          <img src={logo} alt="logo" style={{ height: "15em" }}></img>
        </Row>
        <Row justify="space-around" align="middle">
          <Col sm={2}></Col>
          <Col sm={20} style={{ maxWidth: "30em", textAlign: "center" }}>
            <Card>
              <Form
                labelCol={{ span: 9 }}
                wrapperCol={{ span: 17 }}
                name="basic"
                initialValues={{ remember: true }}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                size={"large"}
                requiredMark={"optional"}
              >
                <Form.Item
                  label="First name"
                  name="firstName"
                  rules={[
                    { required: true, message: "Please input your first name" },
                  ]}
                >
                  <Input />
                </Form.Item>
                <Form.Item
                  label="Last name"
                  name="lastName"
                  rules={[
                    { required: true, message: "Please input your last name" },
                  ]}
                >
                  <Input />
                </Form.Item>
                <Form.Item
                  label="Email"
                  name="email"
                  rules={[
                    { required: true, message: "Please input your email" },
                  ]}
                >
                  <Input />
                </Form.Item>
                <Form.Item
                  label="Password"
                  name="pw"
                  rules={[
                    { required: true, message: "Please choose a password" },
                  ]}
                >
                  <Input.Password />
                </Form.Item>
                <Form.Item
                  label="Confirm Password"
                  name="confirmPw"
                  rules={[
                    { required: true, message: "Please confirm your password" },
                  ]}
                >
                  <Input.Password />
                </Form.Item>
                <Row style={{ height: "1em" }}></Row>
                <Row>
                  <Col sm={9}></Col>
                  <Col xs={8} sm={5}>
                    <Link to="/">
                      <Button type="primary" block ghost>
                        Log in
                      </Button>
                    </Link>
                  </Col>
                  <Col xs={2} sm={1}></Col>
                  <Col xs={14} sm={9}>
                    <Button type="primary" block htmlType="submit">
                      Create account
                    </Button>
                  </Col>
                </Row>
              </Form>
            </Card>
          </Col>
          <Col sm={2}></Col>
        </Row>
      </Content>
    </>
  );
}

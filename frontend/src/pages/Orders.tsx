import { Button, Col, PageHeader } from "antd";
import React from "react";

export default function Orders() {
  return (
    <>
      <Col md={20}>
        <PageHeader
          title="Orders"
          extra={<Button>New Order</Button>}
          style={{ width: "16em" }}
        ></PageHeader>
      </Col>
      <Col md={2}></Col>
      <Col md={6}></Col>
      <Col md={6}></Col>
    </>
  );
}

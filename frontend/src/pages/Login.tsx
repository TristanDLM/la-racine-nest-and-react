import React, { useContext, useState } from "react";
import { Form, Input, Button, Checkbox, Card, Row, Col, Alert } from "antd";
import { Layout } from "antd";
import { Link } from "react-router-dom";
import { LoginDTO } from "../shared/types/users.types";
import axios from "axios";
import { authUrl, baseUrl, hashPw } from "../constants";
import { JwtContext } from "../App";

const { Content } = Layout;

export default function Login() {
  const { setAccessToken: setJwt } = useContext(JwtContext);

  const [loginSuccess, setLoginSuccess] = useState<boolean | undefined>(
    undefined
  );

  const login = (loginDTO: LoginDTO) => {
    axios
      .post(baseUrl + authUrl + "/login", loginDTO)
      .then((res) => {
        setLoginSuccess(true);
        setJwt(res.data.access);
      })
      .catch((err) => {
        setLoginSuccess(false);
      });
  };

  const onFinish = async (values: any) => {
    login({
      username: values.email,
      password: hashPw(values.pw),
    });
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <>
      <Content style={{ minHeight: "100vh", background: "#fafafa" }}>
        <Row
          style={{
            height: "calc(5em + 20vh)",
            alignItems: "flex-end",
            justifyContent: "center",
          }}
        >
          {/* <img src={logo} alt="logo" style={{ height: "15em" }}></img> */}
        </Row>
        <Row justify="space-around" align="middle">
          <Col xs={2} sm={4} md={5}></Col>
          <Col
            xs={20}
            sm={16}
            md={14}
            style={{ maxWidth: "30em", textAlign: "center" }}
          >
            <Card>
              <Form
                labelCol={{ span: 9 }}
                wrapperCol={{ span: 17 }}
                name="basic"
                initialValues={{ remember: true }}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                size={"large"}
                requiredMark={"optional"}
              >
                <Form.Item
                  label="Email"
                  name="email"
                  rules={[
                    { required: true, message: "Please input your email" },
                  ]}
                >
                  <Input />
                </Form.Item>

                <Form.Item
                  label="Password"
                  name="pw"
                  rules={[
                    { required: true, message: "Please input your password" },
                  ]}
                >
                  <Input.Password />
                </Form.Item>
                <Row>
                  <Col sm={7}></Col>
                  <Col sm={17}>
                    <Form.Item name="remember" valuePropName="checked">
                      <div>
                        <Checkbox>Remember me</Checkbox>
                      </div>
                    </Form.Item>
                  </Col>
                </Row>

                <Row>
                  <Col xs={0} sm={9}></Col>
                  <Col xs={14} sm={9}>
                    <Link to="/register">
                      <Button type="primary" block ghost>
                        Create account
                      </Button>
                    </Link>
                  </Col>
                  <Col xs={2} sm={1}></Col>
                  <Col xs={8} sm={5}>
                    <Button type="primary" block htmlType="submit">
                      Log in
                    </Button>
                  </Col>
                </Row>
                {loginSuccess === false && (
                  <Alert
                    message="Wrong email and/or password, please try again"
                    type="error"
                    showIcon
                    style={{ marginTop: "1em" }}
                  ></Alert>
                )}
              </Form>
            </Card>
          </Col>
          <Col xs={2} sm={4} md={5}></Col>
        </Row>
        <Row style={{ height: "20vh" }}></Row>
      </Content>
    </>
  );
}

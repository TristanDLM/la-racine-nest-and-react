import { Card, Col, PageHeader, Row } from "antd";
import React from "react";
import { Calendar } from "antd";
import { useQuery } from "react-query";
import axios from "axios";
import { deliveriesUrl, baseUrl } from "../constants";
import "./deliveries.css";

export default function Deliveries() {
  const { data } = useQuery("deliveries", () =>
    axios.get(baseUrl + deliveriesUrl).then((res) => res.data)
  );
  console.log("data", data);

  const getDay = (dateString: string) => {
    const days = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
    const date = new Date(dateString);
    return (
      <p style={{ margin: "auto" }}>
        {`${days[date.getDay()]} `}
        {"\u00A0"}
        <b>{date.getDate()}</b>
      </p>
    );
  };

  const getMonth = (monthNum: number) => {
    const months = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ];
    return months[monthNum];
  };
  return (
    <>
      <Col md={24}>
        <PageHeader title="Deliveries" style={{ width: "16em" }}></PageHeader>
        {data &&
          Object.keys(data).map((year: any) => (
            <Row style={{ paddingLeft: "1em" }}>
              <Col xs={4}>
                <Card style={{ maxWidth: "10em", height: "100%" }}>{year}</Card>
              </Col>
              <Col xs={14}>
                {Object.keys(data[year]).map((month: any) => (
                  <Row>
                    <Col xs={10}>
                      <Card style={{ height: "100%" }}>{getMonth(month)}</Card>
                    </Col>
                    <Col xs={10}>
                      {Object.values(data[year][month]).map((delivery: any) => (
                        <Row>
                          <Card style={{ height: "100%", width: "100%" }}>
                            {getDay(delivery.date)}
                          </Card>
                        </Row>
                      ))}
                    </Col>
                  </Row>
                ))}
              </Col>
            </Row>
          ))}
      </Col>
    </>
  );
}

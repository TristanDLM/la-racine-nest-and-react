import React, { createContext, useState } from "react";
import "antd/dist/antd.less";
import Login from "./pages/Login";
import Register from "./pages/Register";
import { BrowserRouter, Route } from "react-router-dom";
import Main from "./components/Main";
import {
  useQuery,
  useMutation,
  useQueryClient,
  QueryClient,
  QueryClientProvider,
} from "react-query";

export const JwtContext = createContext({
  accessToken: null as null | string,
  setAccessToken: (token: string) => {},
  refreshToken: null as null | string,
  setRefreshToken: (token: string) => {},
});

const queryClient = new QueryClient();

function App() {
  const [accessToken, setAccessToken] = useState<null | string>(null);
  const setTheAccessToken = (token: string) => setAccessToken(token);
  const [refreshToken, setRefreshToken] = useState<null | string>(null);
  const setTheRefreshToken = (token: string) => setRefreshToken(token);

  const [TokensProvide] = useState({
    accessToken: accessToken,
    setAccessToken: setTheAccessToken,
    refreshToken: refreshToken,
    setRefreshToken: setTheRefreshToken,
  });

  return (
    <QueryClientProvider client={queryClient}>
      <JwtContext.Provider value={TokensProvide}>
        <BrowserRouter>
          {/* <Route exact path="/" component={accessToken ? Main : Login}></Route> */}
          <Route exact path="/" component={Main}></Route>
          <Route exact path="/register" component={Register}></Route>
        </BrowserRouter>
      </JwtContext.Provider>
    </QueryClientProvider>
  );
}

export default App;

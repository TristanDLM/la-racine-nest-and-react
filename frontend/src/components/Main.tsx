import { Card, Col, Layout, Menu, Row } from "antd";
import React, { useState } from "react";
import {
  ShoppingOutlined,
  LineChartOutlined,
  ProfileOutlined,
  BankOutlined,
  CalendarOutlined,
} from "@ant-design/icons";
import Distribution from "../pages/Distribution";
import Orders from "../pages/Orders";
import Stats from "../pages/Stats";
import Finances from "../pages/Finances";
import Deliveries from "../pages/Deliveries";

const { Sider, Content } = Layout;

export default function Main() {
  const pages = [
    {
      key: 1,
      name: "Distribution",
      icon: <ShoppingOutlined />,
      component: <Distribution />,
    },
    {
      key: 2,
      name: "Orders",
      icon: <ProfileOutlined />,
      component: <Orders />,
    },
    {
      key: 3,
      name: "Deliveries",
      icon: <CalendarOutlined />,
      component: <Deliveries />,
    },
    {
      key: 4,
      name: "Stats",
      icon: <LineChartOutlined />,
      component: <Stats />,
    },
    {
      key: 5,
      name: "Finances",
      icon: <BankOutlined />,
      component: <Finances />,
    },
  ];

  const [selectedPage, setSelectedPage] = useState<number>(2);

  return (
    <Layout>
      <Sider breakpoint="sm" collapsedWidth="0" style={{ background: "white" }}>
        <Menu
          defaultSelectedKeys={[`${selectedPage}`]}
          style={{ minHeight: "100vh" }}
          mode="inline"
        >
          <div style={{ height: "calc(33vh - 6em)", display: "block" }}></div>
          {pages.map((item) => {
            return (
              <Menu.Item
                key={item.key}
                style={{ minHeight: "4em", lineHeight: "4em" }}
                icon={item.icon}
                onClick={() => {
                  setSelectedPage(item.key);
                }}
              >
                {item.name}
              </Menu.Item>
            );
          })}
        </Menu>
      </Sider>
      <Content>
        <Row>
          <Col xs={0} lg={8} xxl={16}></Col>
          <Col xs={12} lg={8} xxl={4} style={{ padding: 0 }}>
            <Card style={{ height: "43px", padding: 0 }} size="small">
              <h4>Lydia: 06 09 06 09 06</h4>
            </Card>
          </Col>
          <Col xs={12} lg={8} xxl={4}>
            <Card size="small" title="Next delivery: 30/11/2020">
              Potiron<br></br>
              Aubergine<br></br>
              Poireaux<br></br>
              Taupinambour<br></br>
            </Card>
          </Col>
        </Row>
        {pages.map((page) => {
          return selectedPage === page.key && page.component;
        })}
      </Content>
    </Layout>
  );
}

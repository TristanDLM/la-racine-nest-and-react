-- CreateEnum
CREATE TYPE "deliveryTypes" AS ENUM ('Launch', 'Member', 'One', 'Four', 'Sixteen');

-- CreateTable
CREATE TABLE "refresh_token" (
    "refreshToken" TEXT NOT NULL,

    PRIMARY KEY ("refreshToken")
);

-- CreateTable
CREATE TABLE "user" (
"id" SERIAL,
    "firstName" TEXT NOT NULL,
    "lastName" TEXT NOT NULL,
    "email" TEXT NOT NULL,
    "pwHash" TEXT NOT NULL,
    "role" TEXT NOT NULL,
    "type" TEXT NOT NULL,

    PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "delivery" (
"id" SERIAL,
    "date" TEXT NOT NULL,
    "week" INTEGER NOT NULL,
    "types" "deliveryTypes"[],

    PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "user.email_unique" ON "user"("email");

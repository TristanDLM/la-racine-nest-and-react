import { Body, Controller, Get, Post } from '@nestjs/common';
import { readFile, writeFile } from 'fs';
import { CreateOrderDTO, Order } from '../shared/types/orders.types';
import { v4 } from 'uuid';

@Controller('orders')
export class OrdersController {}

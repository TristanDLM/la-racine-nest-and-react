import { Body, Controller, Get, Logger, Post } from '@nestjs/common';
import { CreateUserDTO } from '../shared/types/users.types';
import { hashSync } from 'bcrypt';
import { UsersService } from './users.service';
import { user } from '@prisma/client';

@Controller('users')
export class UsersController {
  constructor(private service: UsersService) {}

  @Get()
  findAll(): Promise<user[]> {
    Logger.log('GET /users');
    return this.service.findMany();
  }

  @Post()
  create(@Body() createUserDTO: CreateUserDTO): Promise<user> {
    Logger.log(`POST /users with ${JSON.stringify(createUserDTO)}`);
    const newUser: CreateUserDTO = {
      ...createUserDTO,
      pwHash: hashSync(createUserDTO.pwHash, 12),
    };
    return this.service.create(newUser);
  }

  // @Put(':id')
  // updateInfo(
  //   @Param('id') id: string,
  //   @Body() updateUserInfoDTO: UpdateUserInfoDTO,
  // ) {
  //   Logger.log(`PUT /users/${id} with ${JSON.stringify(updateUserInfoDTO)}`);
  //   return this.service.update(updateUserInfoDTO);
  // }
}

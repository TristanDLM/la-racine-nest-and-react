import { Injectable } from '@nestjs/common';
import { CreateUserDTO, UpdateUserInfoDTO } from 'src/shared/types/users.types';
import { user } from '@prisma/client';
import { PrismaService } from 'src/prisma.service';

@Injectable()
export class UsersService {
  constructor(private prisma: PrismaService) {}

  async findMany(): Promise<user[]> {
    return await this.prisma.user.findMany();
  }

  async findUnique(search: Partial<user>): Promise<user> {
    return await this.prisma.user.findUnique({ where: search });
  }

  async create(user: CreateUserDTO): Promise<user> {
    return await this.prisma.user.create({ data: user });
  }
}

import { Controller, Logger, Get, Post, Body } from '@nestjs/common';
import { delivery } from '@prisma/client';
import { DeliveriesService } from './deliveries.service';

@Controller('deliveries')
export class DeliveriesController {
  constructor(private service: DeliveriesService) {}
  @Get()
  async findAll() {
    Logger.log('GET /deliveries');
    return await this.service.findMany();
  }

  @Post()
  create(@Body() delivery: delivery): Promise<delivery> {
    Logger.log(`POST /deliveries with ${JSON.stringify(delivery)}`);
    return this.service.create(delivery);
  }
}

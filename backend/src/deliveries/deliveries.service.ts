import { Injectable } from '@nestjs/common';
import { PrismaService } from 'src/prisma.service';
import { delivery } from '@prisma/client';
import { orderBy } from 'lodash';

@Injectable()
export class DeliveriesService {
  constructor(private prisma: PrismaService) {}
  async findMany() {
    const deliveries = await this.prisma.delivery.findMany();
    //orderBy(deliveries, 'date', 'asc');
    let dateObj = {};
    deliveries.forEach((delivery) => {
      const date = new Date(delivery.date);
      if (!dateObj[date.getFullYear()]) {
        dateObj[date.getFullYear()] = {};
      }
      if (!dateObj[date.getFullYear()][date.getMonth()]) {
        dateObj[date.getFullYear()][date.getMonth()] = [];
      }
      dateObj[date.getFullYear()][date.getMonth()].push(delivery);
    });
    return dateObj;
  }

  async findUnique(search: Partial<delivery>): Promise<delivery> {
    return await this.prisma.delivery.findUnique({ where: search });
  }

  async create(delivery: delivery): Promise<delivery> {
    return await this.prisma.delivery.create({ data: delivery });
  }
}

import {
  Controller,
  Request,
  Post,
  UseGuards,
  Logger,
  Body,
  Res,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Response } from 'express';
import { AuthService } from './auth/auth.service';

@Controller()
export class AppController {
  constructor(private authService: AuthService) {}

  @UseGuards(AuthGuard('local'))
  @Post('auth/login')
  async login(
    @Body() loginDTO,
    @Res({ passthrough: true }) response: Response,
  ) {
    Logger.log(loginDTO);
    const tokens = await this.authService.login(loginDTO);
    if (tokens) {
      response.cookie('Refresh', tokens.refresh, {
        maxAge: 86400000, //24h in ms
        path: '/auth',
        httpOnly: true,
      });
      response.json({ access: tokens.access });
      response.send();
    } else {
      response.status(401);
      response.send();
    }
  }
}

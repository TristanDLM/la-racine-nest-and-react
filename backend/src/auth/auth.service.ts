import { Injectable, Logger } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { compareSync } from 'bcrypt';
import { PrismaService } from 'src/prisma.service';
import { UsersService } from '../users/users.service';
import { jwtConstants } from './constants';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
    private prisma: PrismaService,
  ) {}

  private readonly logger = new Logger(AuthService.name);

  async validateUser(username: string, pass: string): Promise<any> {
    const user = await this.usersService.findUnique({ email: username });
    if (user && compareSync(pass, user.pwHash)) {
      const { pwHash, ...result } = user;
      return result;
    }
    return null;
  }

  async login(loginDTO: any) {
    const result = await this.validateUser(
      loginDTO.username,
      loginDTO.password,
    );
    if (result) {
      console.log('result', result);
      const payload = { id: result.id, email: result.email };
      const refresh = this.jwtService.sign(payload, {
        secret: jwtConstants.refreshSecret,
        expiresIn: '1d',
      });
      this.prisma.refresh_token.create({ data: { refreshToken: refresh } });
      return {
        access: this.jwtService.sign(payload),
        refresh: refresh,
      };
    }
    compareSync(jwtConstants.dummyHash, loginDTO.password);
    return null;
  }
}

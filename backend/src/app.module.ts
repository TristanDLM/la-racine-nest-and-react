import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersController } from './users/users.controller';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { PrismaService } from './prisma.service';
import { DeliveriesModule } from './deliveries/deliveries.module';

@Module({
  imports: [AuthModule, UsersModule, PrismaService, DeliveriesModule],
  controllers: [AppController, UsersController],
  providers: [AppService],
})
export class AppModule {}

export enum OrderType {
  M = "M",
  L = "L",
  U = "U",
  FOUR = "4",
  SIXTEEN = "16",
}

export enum PayMethod {
  CASH = "cash",
  LYDIA = "lydia",
}

export class Order {
  id: string = "";
  userID: string = "";
  type: OrderType = OrderType.U;
  startDate: string = "";
  payMethod: PayMethod = PayMethod.CASH;
  payedAt: Date = new Date();
  receiptSent: boolean = false;
  notes: string = "";
}

export class CreateOrderDTO {
  userID: string = "";
  type: OrderType = OrderType.U;
  startDate: string = "";
  payMethod: PayMethod = PayMethod.CASH;
  notes: string = "";
}

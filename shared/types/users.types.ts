export enum UserRole {
  ADMIN = "admin",
  USER = "user",
}

export enum UserType {
  STUDENT = "student",
  TEACHER = "teacher",
  ADMIN = "admin",
  OTHER = "other",
}

export class CreateUserDTO {
  firstName: string = "";
  lastName: string = "";
  email: string = "";
  pwHash: string = "";
  role: UserRole = UserRole.USER;
  type: UserType = UserType.STUDENT;
}

export class UpdateUserInfoDTO {
  firstName: string = "";
  lastName: string = "";
  email: string = "";
}

export class LoginDTO {
  username: string = "";
  password: string = "";
}

export class UpdateUserPwDTO {
  newPwHash: string = "";
}

export class UpdateUserRoleDTO {
  role: UserRole = UserRole.USER;
}

export class UpdateUserTypeDTO {
  type: UserType = UserType.STUDENT;
}
